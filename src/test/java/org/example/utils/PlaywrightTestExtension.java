package org.example.utils;

import com.microsoft.playwright.*;
import org.example.MainTest;
import org.example.Test2;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.nio.file.Paths;

import static org.example.utils.GeneralUtils.*;

public class PlaywrightTestExtension implements BeforeEachCallback, AfterEachCallback, AutoCloseable {
    private static final Playwright playwright = Playwright.create();
    private static final Browser browser = playwright.chromium().launch(new BrowserType.LaunchOptions()
            .setHeadless(false));

    private static final ContextManager contextManager = ContextManager.getInstance();

    @Override
    public void beforeEach(ExtensionContext context) {
        System.out.println("starting new test context");

        Context browserContext = new Context(browser.newContext(new Browser.NewContextOptions().setRecordVideoDir(Paths.get(getVideoDir(context)))));
        contextManager.register(context, browserContext);

        //TEMP - I am too lazy to actually write injection utils, can just take from our repo
        MainTest.testContext = browserContext;
        Test2.testContext = browserContext;
    }

    @Override
    public void afterEach(ExtensionContext context) {
        System.out.println("closing test context");

        Context browserContext = contextManager.getContext(context);
        contextManager.unregister(context);

        browserContext.close();
    }

    @Override
    public void close() {
        playwright.close();
    }
}
