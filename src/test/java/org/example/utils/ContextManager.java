package org.example.utils;

import org.junit.jupiter.api.extension.ExtensionContext;

import java.util.HashMap;
import java.util.Map;

class ContextManager {
    private static ContextManager contextManager;
    private final Map<ExtensionContext, Context> CONTEXT_MAP = new HashMap<>();

    public static ContextManager getInstance() {
        if (contextManager == null) {
            contextManager = new ContextManager();
        }

        return contextManager;
    }

    public void register(ExtensionContext extensionContext, Context context) {
        CONTEXT_MAP.put(extensionContext, context);
    }

    public void unregister(ExtensionContext extensionContext) {
        CONTEXT_MAP.remove(extensionContext);
    }

    public Context getContext(ExtensionContext extensionContext) {
        return CONTEXT_MAP.get(extensionContext);
    }
}
