package org.example.utils;

import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Page;
import org.example.utils.pages.AbstractPage;
import org.example.utils.pages.dpage.DPage;
import org.example.utils.pages.zpage.ZoPage;

import java.util.Map;
import java.util.function.Function;

public class Context {
    private final BrowserContext browserContext;
    private final PageManager pageManager;

    public Context(BrowserContext browserContext) {
        this.browserContext = browserContext;
        this.pageManager = new PageManager();
    }

    public <T extends AbstractPage> T newPage(Class<T> page) {
        final Map<Class<?>, Function<Page, ?>> PAGE_FACTORY = Map.of(
                ZoPage.class, ZoPage::new,
                DPage.class, DPage::new
        );
        T newPage = (page.cast(PAGE_FACTORY.get(page).apply(browserContext.newPage())));

        pageManager.register(newPage);

        return newPage;
    }

    public void close() {
        browserContext.close();
        pageManager.getPages().forEach(GeneralUtils::attachTestRecording);
    }
}
