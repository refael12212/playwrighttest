package org.example.utils.pages.zpage.utils.forms;

import com.microsoft.playwright.Page;

public class ZForms {
    public final ZSidebarForms sidebarForms;
    public final ZFloatingForms floatingForms;

    public ZForms(Page page) {
        sidebarForms = new ZSidebarForms(page);
        floatingForms = new ZFloatingForms(page);
    }
}
