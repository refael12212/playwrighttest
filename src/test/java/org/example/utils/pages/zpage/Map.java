package org.example.utils.pages.zpage;

import com.microsoft.playwright.Locator;
import org.example.utils.exceptions.MapNotEnabledException;

public class Map {
    private boolean isEnabled;

    private Locator canvas;

    public Map() {
        isEnabled = false;
    }

    void setEnabled(boolean enabled) {
        isEnabled = enabled;

        if (isEnabled) {
            //TODO GET CANVAS
        } else {
            this.canvas = null;
        }
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void doSomething() {
        if (!isEnabled) {
            throw new MapNotEnabledException();
        }

        //TODO do something with map
    }
}
