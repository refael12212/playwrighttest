package org.example.utils.pages.zpage.utils.sidebar;

import com.microsoft.playwright.Page;
import org.example.utils.pages.utils.Sidebar;

import java.util.Map;
import java.util.function.Consumer;

public class ZSidebar extends Sidebar<ZSidebarItem> {
    private static final Map<ZSidebarItem, Consumer<Page>> OPEN_ITEM_MAP = Map.of(
        ZSidebarItem.E_DETAILS, (page) -> {
            /*
            OPEN E DETAILS as in: click on ent, then click on e details
             */
            },
            ZSidebarItem.R_DETAILS, (page) -> {
                // OPEN R DETAILS as in: click on ent, then click on r details
            }
    );

    public ZSidebar(Page page) {
        super(OPEN_ITEM_MAP, page);
    }

    @Override
    public void close() {
        // close sidebar
    }

    @Override
    protected void waitForLoadingToStop() {
        // wait
    }
}
