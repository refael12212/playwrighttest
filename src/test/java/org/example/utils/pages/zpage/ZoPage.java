package org.example.utils.pages.zpage;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.TimeoutError;
import org.example.utils.exceptions.CouldNotNavigateToPageException;
import org.example.utils.pages.AbstractPage;
import org.example.utils.pages.zpage.utils.forms.ZForms;
import org.example.utils.pages.zpage.utils.sidebar.ZSidebar;

import java.util.Optional;

public class ZoPage extends AbstractPage {
    private static final String NO_MAP_POSTFIX = "";

    public final ZSidebar sidebar;
    public final ZForms forms;

    public final Map map = new Map();

    public ZoPage(Page page) {
        super("https://playwright.dev/java/docs/videos", "Videos | Playwright Java", page, "ZPage");

        sidebar = new ZSidebar(page);
        forms = new ZForms(page);
    }

    public void setMapEnabled(boolean mapEnabled) {
        map.setEnabled(mapEnabled);
        navigate();
    }

    @Override
    protected void navigate() {
        try {
            pageElement.navigate(getUrl());
            pageElement.waitForCondition(() -> pageElement.title().equals(title));
        } catch (TimeoutError timeoutError) {
            throw new CouldNotNavigateToPageException(this, pageElement.url(), pageElement.title());
        }
    }

    private String getUrl() {
        return Optional.ofNullable(map)
                .map(Map::isEnabled)
                .filter(isEnabled -> isEnabled)
                .map(isEnabled -> url)
                .orElse(url + NO_MAP_POSTFIX);
    }
}
