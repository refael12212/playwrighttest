package org.example.utils.pages.zpage.utils.forms;

import com.microsoft.playwright.Page;

public class ZFloatingForms {
    public final SForm sForm;
    public final FForm fForm;
    public ZFloatingForms(Page page) {
        sForm = new SForm(page);
        fForm = new FForm(page);
    }
}
