package org.example.utils.pages.zpage.utils.forms;

import com.microsoft.playwright.Page;
import org.example.utils.pages.utils.FloatingForm;

public class SForm extends FloatingForm {
    protected SForm(Page page) {
        super(page);
    }

    public void open(String displayName) {}

    @Override
    public void close() {

    }
}
