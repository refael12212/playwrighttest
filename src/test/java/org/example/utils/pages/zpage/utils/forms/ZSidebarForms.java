package org.example.utils.pages.zpage.utils.forms;

import com.microsoft.playwright.Page;

public class ZSidebarForms {
    public final EDetails eDetails;
    public final RDetails rDetails;

    public ZSidebarForms(Page page) {
        eDetails = new EDetails(page);
        rDetails = new RDetails(page);
    }
}
