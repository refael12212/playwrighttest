package org.example.utils.pages.utils;

import com.microsoft.playwright.Page;

public abstract class FloatingForm {
    private final Page page;

    protected FloatingForm(Page page) {
        this.page = page;
    }

    public abstract void close();
}
