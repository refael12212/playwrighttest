package org.example.utils.pages.utils;

import com.microsoft.playwright.Page;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Consumer;

public abstract class Sidebar<PageSidebarItem> {
    protected final Map<PageSidebarItem, Consumer<Page>> openItemMap;
    protected final Page page;

    protected Sidebar(Map<PageSidebarItem, Consumer<Page>> openItemMap, Page page) {
        this.openItemMap = openItemMap;
        this.page = page;
    }

    public void openItem(PageSidebarItem item) {
        close();
        openItemMap.get(Optional.of(item)
                .filter(openItemMap::containsKey)
                .orElseThrow(NoSuchElementException::new))
                .accept(page);
        waitForLoadingToStop();
    }

    public abstract void close();

    protected abstract void waitForLoadingToStop();
}
