package org.example.utils.pages.dpage;

import com.microsoft.playwright.Page;
import org.example.utils.pages.AbstractPage;

public class DPage extends AbstractPage {

    public DPage(Page page) {
        super("https://playwright.dev/java/docs/videos", "Videos | Playwright Java",  page, "DPage");
    }
}
