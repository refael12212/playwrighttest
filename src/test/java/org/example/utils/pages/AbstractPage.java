package org.example.utils.pages;

import com.microsoft.playwright.Page;
import com.microsoft.playwright.TimeoutError;
import org.example.utils.exceptions.CouldNotNavigateToPageException;

public class AbstractPage {
    protected final String url;
    protected final String title;
    protected final Page pageElement;
    protected final String name;

    public AbstractPage(String url, String title, Page page, String name) {
        this.url = url;
        this.pageElement = page;
        this.name = name;
        this.title = title;

        navigate();
    }

    public Page getPageElement() {
        return pageElement;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Name: \"%s\", Url: \"%s\", Title: \"%s\"".formatted(name, url, title);
    }

    protected void navigate() {
        try {
            pageElement.navigate(url);
            pageElement.waitForCondition(() -> pageElement.title().equals(title));
        } catch (TimeoutError timeoutError) {
            throw new CouldNotNavigateToPageException(this, pageElement.url(), pageElement.title());
        }
    }
}
