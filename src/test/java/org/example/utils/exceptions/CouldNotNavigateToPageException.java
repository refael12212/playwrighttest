package org.example.utils.exceptions;

import org.example.utils.pages.AbstractPage;

public class CouldNotNavigateToPageException extends RuntimeException {
    public CouldNotNavigateToPageException(AbstractPage page, String actualUrl, String actualTitle) {
        super("Could not navigate to page %s - Actual Url was: \"%s\" and the actual title was: \"%s\"".formatted(page, actualUrl, actualTitle));
    }
}
