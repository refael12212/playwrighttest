package org.example.utils.exceptions;

public class MapNotEnabledException extends RuntimeException {
    public MapNotEnabledException() {
        super("Tried to call map function when map was disabled");
    }
}
