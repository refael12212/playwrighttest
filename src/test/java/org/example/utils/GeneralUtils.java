package org.example.utils;

import io.qameta.allure.Allure;
import org.example.utils.pages.AbstractPage;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class GeneralUtils {
    private static final String BASE_VIDEOS_PATH = "videos/";

    public static String getVideoDir(ExtensionContext extensionContext) {
        return "%s/%s/%s/".formatted(BASE_VIDEOS_PATH, extensionContext.getRequiredTestClass().getName(), extensionContext.getRequiredTestMethod().getName());
    }

    public static void attachTestRecording(AbstractPage page) {
        try {
            Path path = page.getPageElement().video().path();

            InputStream vid = Files.newInputStream(path, StandardOpenOption.DELETE_ON_CLOSE);
            //Make sure to configure the videos to be deleted after closure to make sure that no duplicates of videos happen in test rerun
            Allure.addAttachment(page.getName(), "video/webm", vid, "webm");
            // webm is the file extension needed to show video in allure

            vid.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
