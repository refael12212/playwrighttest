package org.example.utils;

import org.example.utils.pages.AbstractPage;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class PageManager {
    private final Set<AbstractPage> pages;

    public PageManager() {
        pages = new HashSet<>();
    }

    public void register(AbstractPage page) {
        pages.add(page);
    }

    public Set<AbstractPage> getPages() {
        return pages;
    }
}
