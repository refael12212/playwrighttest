package org.example;

import org.example.utils.Context;
import org.example.utils.PlaywrightTestExtension;
import org.example.utils.exceptions.MapNotEnabledException;
import org.example.utils.pages.dpage.DPage;
import org.example.utils.pages.zpage.ZoPage;
import org.example.utils.pages.zpage.utils.sidebar.ZSidebarItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(PlaywrightTestExtension.class)
public class MainTest {
    public static Context testContext; //Generated in PlaywrightTestExtension beforeEach per test

    @Test
    void cc() {
        ZoPage zPage = testContext.newPage(ZoPage.class);
        zPage.sidebar.openItem(ZSidebarItem.E_DETAILS);
        zPage.forms.sidebarForms.eDetails.doSomething();
        zPage.sidebar.openItem(ZSidebarItem.R_DETAILS);
        zPage.forms.floatingForms.fForm.addEntity("24.5");
        zPage.forms.floatingForms.fForm.addEntity("12.6");
        zPage.forms.floatingForms.fForm.fuse();
        zPage.setMapEnabled(true);
        zPage.map.doSomething();


        DPage page2 =  testContext.newPage(DPage.class);
    }

    @Test
    void dd() {
        ZoPage zPage = testContext.newPage(ZoPage.class);
        zPage.sidebar.openItem(ZSidebarItem.E_DETAILS);
        zPage.forms.sidebarForms.eDetails.doSomething();
        zPage.sidebar.openItem(ZSidebarItem.R_DETAILS);
        zPage.forms.floatingForms.fForm.addEntity("24.5");
        zPage.forms.floatingForms.fForm.addEntity("12.6");
        zPage.forms.floatingForms.fForm.fuse();
        Assertions.assertThrows(MapNotEnabledException.class, zPage.map::doSomething);

        DPage page2 =  testContext.newPage(DPage.class);
    }
}